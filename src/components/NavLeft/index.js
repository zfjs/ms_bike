import React from 'react'
// import MenuConfig from '../../config/menuConfig'
import { Menu } from 'antd'
import './index.less'


// const { SubMenu } = Menu;


export default class NavLeft extends React.Component{

  // componentWillMount() {
  //   const menuTreeNode = this.renderMenu()
  // }

  renderMenu() {

  }

  render () {
    console.log(Menu);
    
    return (
      <div>
        <div className='logo'>
          <img src="/assets/logo-ant.svg" alt="" />
          <h1>MS BIKE</h1>
        </div>
        <Menu theme="dark" mode="vertical">
          <Menu.SubMenu
            key="sub4"
            title={
              <span>
                <span>Navigation Three</span>
              </span>
            }
          >
            <Menu.Item key="9">Option 9</Menu.Item>
            <Menu.Item key="10">Option 10</Menu.Item>
            <Menu.Item key="11">Option 11</Menu.Item>
            <Menu.Item key="12">Option 12</Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </div>
    )
  }
}