import React from 'react'
import Child from './Child'
import './abc.less'
import { Button } from 'antd'


export default class Life extends React.Component{

  state = {
    count: 0
  }

  handleMiu = () => {
    this.setState({
      count: this.state.count - 1
    })
  }

  handleAdd = () => {
    this.setState({
      count: this.state.count + 1
    })
  }


  render() {
    return (
      <div>
        <h2 className="color-red">{this.state.count}</h2>
        <button onClick={this.handleAdd}>+</button>
        <button onClick={this.handleMiu}>-</button>
        <hr/>
        <Button>123</Button>
        <Child count={this.state.count} />
      </div>
    )
  }
}