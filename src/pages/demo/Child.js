import React from 'react'


export default class Child extends React.Component{

  componentWillMount () {
    console.log('child will mount');
    
  }

  componentDidMount () {
    console.log('child did mount');
  }

  componentWillReceiveProps(newProps) {
    console.log('will receive props ' + newProps.count);
  }

  shouldComponentUpdate() {
    console.log('should update');
    return true
  }

  componentWillUpdate() {
    console.log('will update');
  }

  componentDidUpdate() {
    console.log('did update');
    
  }

  render() {
    return (
      <div>
        <h2>{this.props.count}</h2>
      </div>
    )
  }
}